


          Concerto in G Minor, Op.10, No. 2, "Night", RV 439 
                     Le Cene 544, Amsterdam [c.1728] 



Movement 1 - Largo in 3/4  This movement was printed out in one flat, 
          like the manuscript, & also in two flats, like it a modern 
          edition. I think Eleanor would like it printed in one flat.  

   Meas.11\1 - E4 in Violin 2.  Added an editorial flat to make Eb as in 
          the bass.  Giordano RV 104 has an Eb in the lower voices.  
          Malipiero for RV 439 has an editorial Eb.  
   Meas.18\1 - E4 in Violin 1 and 6/4 figure in bass.  Left the way it 
          was.  Considered adding an editorial flat and changing the 
          figure to 6b/4.  Reasoning - 1) It is less jarring to remain in 
          minor. 2) The copier was leaving out flats on Es in other 
          sections of this work. 3) Meas.23-24 are a repeat of m. 26-27 
          Perhaps he intended m.17-18 to be the same as m.20-21. These 
          are not compelling enough reasons for me to change these notes.  
          Malipiero has Eb.  Giordano RV 104 has a questionable flat and 
          if the flat there is correct, the rhythm is wrong.                   
          I think that Eleanor should be consulted on this one.  
   Meas.28\1 - Track 4 added a fermata.  Fermatas on this last note in 
          all other parts.  


Movement 2 - Fantasmi - Presto, Largo.  Printed out in one flat like Le 
          Cene.  The files for it to be printed out in 2 flats are in 
          OP10-2GM, should that be desired.  

   Meas.22\2 - Bb5 in Violin 1.  Added an editorial natural on B5.  In 
          all other parts there are B naturals.  Malipiero also has 
          an editorial natural on this note and B naturals in the other 
          voices.  RV 104 does not have any naturals.  I think the Bb 
          makes more musical sense. (Dec.08-either seems ok. The Bs in 
          all parts of the first note of the Largo, m.23, are strange.) 

Movement 3 - Presto  This movement is in F Major and is printed in one 
          flat. Important for Eleanor to make judgements on some E flats 
          in this movement.  

   Meas.7\1 - Violin 1.  Entered an editorial Eb. No flat on E5 in Le 
          Cene or Malipiero but Giordano RV 104 has an Eb.  It is leading 
          down to D5 in the next measure and makes more musical sense 
          than E5 natural.          
   Meas.13\3,5 - Viola has C3 in Le Cene and Giordano, but Malipiero 
          has A3s! (Wrong. Probably the error of a copyist.) 
   Meas.25\12 - Flute. Entered editorial E5 natural.  Malipiero for 
          RV 439 also had Ef5 but added an editorial natural for the 
          first E in the next measure.  In Giordano for RV 104 there was 
          no flat and Ephrikian in Ricordi for RV 104 also had no flat.  
          I have a musical preference for the natural but it could be 
          either. 
   Meas.39\1 - Violin 2. Entered an editorial B4. A4 is wrong and           
          Malipiero had an editorial G4 with a note about the manuscript 
          having an A4. The harmony is the same in Giordano but the 
          structure of the music is different and Giordano has a D5 
          8th and B4 16ths for this track in this measure (m.42).  A G4 
          would just double the viola so I chose the B4 to fit into the           
          harmony.  Eleanor may want to change this.  
   Meas.46\1 - Violin 2. Entered an editorial E natural.  Important for 
          Eleanor to look at this for neither manuscript had a flat on 
          this note but it was preceded by arpeggios with E flats, so 
          one might think it should end in minor with an Eb. Malipiero 
          has an editorial natural for RV 439 ending in major and 
          Ephrikian has an editorial flat, ending RV 104 in minor!  


Movement 4 - Il Sonno - Largo   This movement was written and printed 
          out in three flats.  

   Meas.17\3 - Ab4 in Violin 2.  Added editorial natural to make it A4.  
          Harmonic reasons. Also an A natural in the viola. An oversight 
          leaving out the natural?  Malipiero has an editorial natural.  
          The B4 before this note and the B4 after (next meas) both 
          have naturals in front of them as does Malipiero for RV 439.  


Movement 5 - Allegro   Printed out in one flat like manuscript and also 
          in two flats.  I think Eleanor wants it printed in one flat.  
            Meas. 35-45 were notated in whole note chords in Le Cene 
          and I have written them out in the arpeggiated form in which 
          they would be played.  

   Meas.16\5 - E4 in Viola.  Entered editorial flats to make them Eb4, 
          (descending minor scale.)  Perhaps another example of a 
          copyist forgetting to add a flat since there was only one 
          flat in the key signature.  Also it seems like it is easier 
          to miss accidentals when writing parts than scores.  Giordano 
          has an Eb.  Malipiero has an editorial flat for RV 439.  
   Meas.18\5 - Identical to measure 16.  Same comments apply.  



NB  Meas.16\5 refers to the 5th note of the 16th measure 
