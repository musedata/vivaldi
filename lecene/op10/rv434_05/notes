


                Concerto in F Major, Op.10, No. 5, RV 434 
                     Le Cene 544, Amsterdam [c.1728] 



Movement 1 - Allegro ma non tanto     Fermata questions in this movement.  
          Meas.1-12 and Meas.68+ to 79 (end) have the same material, with 
          the exception of measures 5+ to 6+ which are slightly different 
          from measure 73 for tracks 1-2. (In m.73 they are down a third 
          from measures 5+ to 6+.)  A scribe's error?  Le Cene seems to 
          have written out what Vivaldi most likely intended as a Da Capo 
          al Segno (fermata).  There were fermatas in all tracks at m.12 
          and in tracks 1-4 at the end.  I added a fermata at the end in 
          track 5.  

   Meas.5\4 - Violin 1 was written Bb5.  Entered C6 like Flute (unison) 
          and like meas.72.  RV 442 has Violin 1 in unison with Flute.  
   Meas.14\8-10 - The Flute's last 3 notes were Bf5-A5-Bf5 as in the 
          previous turn.  The harmony supports a descending line (see 
          m.2-3) so the turn was lowered and editorial notes A5-G5-A5 
          were entered.  Giordano RV 442 and Riccordi have A5-G5-A5.  
   Meas.29\2 - Viola has E4.  Entered an editorial F4 (unison with tr.5).  
          Giordano RV 442 and Ricordi have F4.  
   Meas.45 - Viola has rest-A3-rest-A3 in eighths the first half of this 
          measure. RV 442 has A3-rest-A3-rest the first half of meas.45.  
          A possible scribe's error in Le Cene, continuing the pattern       
          of the previous measure which began with a rest. Entered                      
          editorial notes and rests like RV 442.  
   Meas.51\3 - Violin 1 has F5.  Left as it was. Ricordi has G5. Harmony 
          doesn't change until the second half of the measure so F5 is 
          more appropriate.  F5 in Giordano RV 442.  
   Meas.70\4 - Flute has Bb5. Entered C6, unison with Violin 1 and like 
          meas.2.  In Giordano the note was C6 for there was a Da Capo at 
          meassure 68.                      
   Meas.78\1-4 - Violin 2 was E5-E5-Eb5-Eb5. Entered Eb5-Eb5-E5-E5 like 
          measure 10.  Misplaced flat in Le Cene?  Should be like the 
          ascending chromatic passage in m.76 (tr.1-2).  Giordano has a 
          Da Capo so m.76-78 and m.8-10 have the same material.  


Movement 2 - Largo Cantabile     Fermata questions in this movement.  
          Same musical material in meas.1-6 as in meas.27-32.  There was 
          a fermata in measure 6 only in track 1 and at the end only in 
          track 2.  Giordano RV 442 has a Da Capo al Segno (fermata).  
          In RV 442, Vivaldi has written that this movement should be 
          one tone higher.  RV 442 & Ricordi have notated this movement 
          in 3 flats with the top voice starting on C4.  Le Cene has 
          written it in 2 flats and begins on D4.  

   Meas.2\4 - Track 4 has Eb4.  Entered an editorial E4. No natural sign 
          in Le Cene. An oversight? E4 makes more harmonic sense. E4 in 
          Giordano. Ricordi has an E4 with an editorial (b).  
   Meas.3\1 - Track 4 has Bb3.  Entered editorial A3 as in the identical 
          passage at meas.29. Giordano RV 442 has A3 as does Ricordi.  
   Meas.8\8 - Track 1 has F5. Entered F5.  RV 442 has F#5. Possible 
          oversight in leaving out a #.  
   Meas.8\4 - Tracks 2=3 have Bf3 which I entered. The manuscript RV 442 
          has been altered at this point and a C4 seems more likely than 
          Bf3.  Ricordi has a C4.  
   Meas.15\6-8 - Track 1 has B5-C6-D6 like meas.13-14! Entered editorial 
          C#6-D6-E6 continuing the ascending pattern with the first note 
          starting the same as the C# in track 5.  RV442 & Ricordi have 
          C#6-D6-E6 also.  
   Meas.16\1-3 - Track 1.  A repeat of meas.15 with the same correction.  
   Meas.20\2 - Track 1 has Ef5. (At odds with the strings but a different 
          timbre so possibly all right, but I think it was an error in 
          transcribing it up a tone.)  Entered an editorial natural to 
          make it E5.  E natural in Giordano RV 442 and Ricordi.  
   Meas.28\4 - Track 4 has Eb4. Entered editorial E4. Same as in meas.2 
   Meas.31\2 - Track 4 has F4. Entered editorial F#4.  Identical to m.5.  
          Another careless transcription error in forgetting a "#" ?  


Movement 3 - Allegro    I feel that this is a case where Le Cene didn't  
          write out the Da Capo and forgot to mention it.  Added [Da Capo 
          al Segno].  Giordano RV 442 has a Da Capo and Ricordi has it 
          written out.  

   Meas.33\1 - Violin 2 has A3.  Entered an editorial G3.  The violins 
          are in unison here and there is also a G in the flute.  
          Giordano RV 442 and Ricordi have G3.  
   Meas.42\4 - Track 1 has C#6. Entered an editorial natural. Oversight, 
          or the practice of the day in this kind of figure?  Giordano 
          RV 442 also does not have a natural but Ricordi has one and it 
          is not even an editorial one.  A C#6 is harmonically jarring.  
   Meas.43\4 - Tr.2 has C#6. Entered an editorial natural. Same as m.42 
   Meas.53\3 - Track 5  has a 6/5 figure which I question. The notes 
          there are E5, E5, G5, A4, & G2.  Le Cene is the only edition 
          with figures and I did not change it, but that beat is a little 
          unsettling harmonically.  In track 4, Le Cene and RV 442 have 
          four 8th notes on A4 but Ricordi has a Bf4 on the third one 
          with a footnote that "Ms:La".                                          
   Meas.89\1 - Track 1 has A5. Giordano RV 434 and Ricordi have C6.  
          Either way works.  
