(C) 2004, 2004 Center for Computer Assisted Research in the Humanities.
ID: {viv/roger/op4/rv279/stage2/02/01}
TIMESTAMP: SEP/24/2004 [md5sum:974b7c4fd816495d01aeb59558b24873]
08/27/10 Fran Bennion
WK#:04,02     MV#:2
RV 279 / Estienne Roger (No. 399), Amsterdam [facs.]
Vivaldi Op.4, No 2 in E Minor
Movement 2 - II.
Violino di Concertino

Group memberships: score
score: part 1 of 5
&1 
This version of Mvt.2 guesses that the "mysterious" 
small note above the leger line above the E5 in Roger 
is a F#5 grace note to the 2nd 8th note of the 3rd beat. 
&1 
Pv4.02
$   Q:8  K:1  T:1/1  C:4
*               D +     Largo
P    C25:f37  C17:Y-33x-33
G5     4      1 e     d  [
G5     4      1 e     d  =
*               G       p
P    C17:Y74
G4     4      1 e     d  =
G4     4      1 e     d  ]
*               G       f
P    C17:Y79
F#5    4      1 e     d  [
F#5    4      1 e     d  =
*               G       p
P    C17:Y79
F#4    4      1 e     d  =
F#4    4      1 e     d  ]
measure 2
*               G       f
P    C17:Y72
G#5    4      1 e #   d  [
G#5    4      1 e     d  =
*               G       p
P    C17:Y73
G#4    4      1 e #   d  =
G#4    4      1 e     d  ]
*               G       f
P    C17:Y72
A#5    4      1 e #   d  [
A#5    4      1 e     d  =
*               G       p
P    C17:Y73
A#4    4      1 e #   d  =
A#4    4      1 e     d  ]
measure 3
*               G       f
P    C17:Y68
B5     4      1 e     d  [
B5     4      1 e     d  =
*               G       p
P    C17:Y69
B4     4      1 e     d  =
B4     4      1 e     d  ]
*               G       f
P    C17:Y73
G5     4      1 e     d  [
G5     4      1 e     d  =
*               G       p
P    C17:Y75
G4     4      1 e     d  =
G4     4      1 e     d  ]
measure 4
*               G       f
P    C17:Y77
F#5    4      1 e     d  [
F#5    4      1 e     d  =
*               G       p
P    C17:Y77
F#4    4      1 e     d  =
F#4    4      1 e     d  ]
*             1 D +     Solo
P    C25:f31  C17:Y-30
*               G       f
P    C17:Y60
B5     4      1 e     d  [
G5     2      1 s     d  =[
F#5    2      1 s     d  ]]
E5     4      1 e     d  [
B5     4      1 e     d  ]
measure 5
C6     2      1 s     d  [[    (
A5     2      1 s     d  ==    )
F#5    2      1 s     d  ==    (
E5     2      1 s     d  ]]    )
D#5    2      1 s #   d  [[    (
C6     2      1 s     d  ==    )
B5     2      1 s     d  ==    (
A5     2      1 s     d  ]]    )
G5     2      1 s     d  [[    (
A5     2      1 s     d  ==
B5     2      1 s     d  ==    )
G5     2      1 s     d  ]]
E5     8      1 q     d
measure 6
*             1 D +     Tutti
P    C25:f31  C17:Y-20
G5     4      1 e     d  [
G5     4      1 e     d  =
*               G       p
P    C17:Y73
G4     4      1 e     d  =
G4     4      1 e     d  ]
*               G       f
P    C17:Y78
F#5    4      1 e     d  [
F#5    4      1 e     d  =
*               G       p
P    C17:Y77
F#4    4      1 e     d  =
F#4    4      1 e     d  ]
measure 7
*             1 D +     Solo
P    C25:f31  C17:Y-38
*               G       f
P    C17:Y60
B5     8-     1 q     d        -
B5     2      1 s     d  [[
G5     1      1 t     d  ==[   (
A5     1      1 t     d  ===
B5     1      1 t     d  ===
G5     1      1 t     d  ===
A5     1      1 t     d  ===
B5     1      1 t     d  ]]]   )
E5     2      1 s     d  [[    (
B5     2      1 s     d  ==    )
E6     2      1 s     d  ==    (
B5     2      1 s     d  ]]    )
C6     2      1 s     d  [[    (
A5     2      1 s     d  ==    )
F5     2      1 s n   d  ==    (
E5     2      1 s     d  ]]    )
measure 8
D#5    8-     1 q #   d        -
D#5    2      1 s     d  [[
D#5    1      1 t     d  ==[   (
E5     1      1 t     d  ===
F#5    1      1 t #   d  ===
G5     1      1 t     d  ===
A5     1      1 t     d  ===
B5     1      1 t     d  ]]]   )
D#5    2      1 s     d  [[
D#5    1      1 t     d  ==[   (
E5     1      1 t     d  ===
F#5    1      1 t     d  ===
G5     1      1 t     d  ===
A5     1      1 t     d  ===
B5     1      1 t     d  ]]]   )
B4     2      1 s     d  [[
C#5    1      1 t #   d  ==[   (
D#5    1      1 t     d  ===
E5     1      1 t     d  ===
F#5    1      1 t     d  ===
G5     1      1 t     d  ===
A5     1      1 t     d  ]]]   )
measure 9
G5     2      1 s     d  [[
F#5    2      1 s     d  =]
E5     4      1 e     d  ]
rest   2      1 s
E6     2      1 s     d  [[
C#6    2      1 s #   d  ==
B5     2      1 s     d  ]]
A#5    8-     1 q #   d        -
A#5    2      1 s     d  [[
C#6    2      1 s     d  ==
F#5    2      1 s     d  ==
E5     2      1 s     d  ]]
measure 10
D5     2      1 s n   d  [[
F#5    2      1 s     d  ==
B5     2      1 s     d  ==
B4     2      1 s     d  ]]
C#5    6      1 e.#   d  [     t
B4     2      1 s     d  ]\
B4     8-     1 q     d        -
B4     2      1 s     d  [[
C#5    1      1 t     d  ==[   (
D#5    1      1 t #   d  ===
E5     1      1 t     d  ===
F#5    1      1 t     d  ===
G#5    1      1 t #   d  ===
A5     1      1 t     d  ]]]   )
measure 11
B5     3      1 s.    d  [[
A5     1      1 t     d  ==\   (
G#5    3      1 s.#   d  ==    )
F5     1      1 t n   d  ]]\   (
E5     4      1 e     d  [     )
D5     4      1 e     d  ]
C#5    8-     1 q #   d        -
C#5    1      1 t     d  [[[
A4     1      1 t     d  ===   (
B4     1      1 t     d  ===
C#5    1      1 t     d  ===
D5     1      1 t     d  ===
E5     1      1 t     d  ===
F#5    1    &11 t     d  ===
&1
Last two notes return to key signature (going
to G+).  Made EDITORIAL F# and G natural.
&1
G5     1    &11 t n   d  ]]]   )
measure 12
A5     3      1 s.    d  [[
G5     1      1 t     d  ==\   (
F#5    3      1 s.    d  ==    )
E5     1      1 t     d  ]]\   (
D5     4      1 e     d  [     )
C5     4      1 e n   d  ]
B4     8      1 q     d
rest   2      1 s
B5     2      1 s     d  [[
G5     2      1 s     d  ==
E5     2      1 s     d  ]]
measure 13
F5     2      1 s n   d  [[
A5     1      1 t     d  ==[   (
G#5    1      1 t #   d  ==]   )
A5     2      1 s     d  ==
C6     1      1 t     d  ==[   (
B5     1      1 t     d  ]]]   )
C6     2      1 s     d  [[    (
A5     2      1 s     d  ==    )
F5     2      1 s     d  ==    (
A5     2      1 s     d  ]]    )
gE5    6      1 6     u        (
D#5    8-     1 q #   d        -)
D#5    2      1 s     d  [[
A5     2      1 s     d  ==
G5     2      1 s n   d  ==
&1
Last two notes return to key signature (going
to E minor).  Made EDITORIAL F# & G natural.
&1
F#5    2      1 s #   d  ]]
measure 14
G5     4      1 e     d  [
F#5    2      1 s     d  =[
E5     2      1 s     d  ]]
D#5    6      1 e.#   d  [     (t
P    C33:Y-12
E5     2      1 s     d  ]\    )
E5     8      1 q     d
rest   1      1 t
G5     1      1 t     d  [[[   (
F#5    1      1 t     d  ===
E5     1      1 t     d  ===   )
B5     1      1 t     d  ===   (
G5     1      1 t     d  ===
F#5    1      1 t     d  ===
E5     1      1 t     d  ]]]   )
measure 15
E6     1      1 t     d  [[[
D6     1      1 t n   d  ===   (
C6     1      1 t     d  ===   )
B5     1      1 t     d  ===   (
A5     1      1 t     d  ===   )
G5     1      1 t     d  ===   (
F#5    1      1 t     d  ===   )
E5     1      1 t     d  ]]]
D#5    6      1 e.#   d  [      t
E5     2      1 s     d  ]\
*             1 D +     Tutti
P    C25:f31  C17:Y-33x-7
E5     4      1 e     d  [
gF#5   6    &11 6     u          & (
&1
In Roger space is cramped here and the grace note
sits above the staff just to the left of the 2nd E5.
The pitch is uncertain and the slur is EDITORIAL.
(Possibly just a suggestion for the soloist to do
their own ornamentation, an alternate note of some
sort, or a grace note on another pitch?)
&1
G5     4      1 e     d  ]       & )
*               G       p
P    C17:Y64
G4     4      1 e     u  [
G4     4      1 e     u  ]
measure 16
*               G       f
P    C17:Y78
F#5    4      1 e     d  [
F#5    4      1 e     d  =
*               G       p
P    C17:Y78
F#4    4      1 e     d  =
F#4    4      1 e     d  ]
*               G       f
P    C17:Y78
F#5    4      1 e     d  [
F#5    4      1 e     d  =
*               G       p
P    C17:Y78
F#4    4      1 e     d  =
F#4    4      1 e     d  ]
measure 17
G4    32      1 w               F
mheavy2
/END
