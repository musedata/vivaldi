      (Old) NOTES on Vivaldi Opus 4 - Violin Concertos I - XII 
    for Violino di Concertino, Vn.1 & 2, Viola, & Basso continuo.  

  The facsimile of Vivaldi's La Stravaganza Op.4 has five part-books: 
     Violino di Concertino, Violino Prima, Violino Seconda, Alto 
     Viola, and Organo e Violoncello (with figures above the notes).  
     This facsimile is based on the copy of the publication done by 
      Chez Estienne Roger Marchand Libraire, Amsterdam, No.399, 
         which belongs to the Royal Academy of Music, London.  

                         (Sep. 14, 2010) 

      The notes, their values, stem directions, rests, beamings, and 
    fermatas were scanned with a high degree of accuracy. Accidentals, 
    especially in ( ), were not always scanned accurately - sharps 
    often scanned as naturals, naturals as sharps, and fermatas 
    occasionally missed altogether. "&0" was entered in col.13-14 
    of note records indicating editorial material from the source, 
    in this edition often a cautionary accidental.  "&1" = a CCARH 
    editorial. On the next line "@1" + an explanation of the problem.  
      Dynamics were scanned as Musical Directions with "*" in col.1, 
    "G" in column 17, & the dynamic in column 25.  Vertical placement 
    was adjusted in "ss2ed".  Roger also had many fewer dynamic 
    markings and slurs than the scanned edition, the scanner picking 
    up the Print suggestions "P C32:o" or "u" for each one. The scan 
    placed fermatas, trills, and staccato dots in column 32 instead 
    of col.33+.  Roger had no staccato marks in this Concerto & the 
    Ri_scan had quite a few, but not as many as Concertos I. & II.  
      The facsimile had a light double bar between movements, There 
    were no repeat signs in Opua 4, except for the Allegro of 
    Concerto III.  After each Concerto the light double bar was 
    followed by several vertical lines of diminishing heights.   
    There were holds at the end of each movement except for 
    Concertos VIII and X, which to me are particularly interesting,        
    in the way the Adagios functioned differently than the other 
    middle movements.  Concerto VIII was particularly interesting 
      "&0" was entered in col.13-14 to indicate that there was 
    editorial material from the source on that line. "&1" = a CCARH 
    editorial where something was changed from the source.  On the 
    lines between "&0" or "&1" in the 1st column there would often be 
    a comment on the situation.  Editorial accidentals which were 
    added appeared in smaller fonts & notes were made in the files.  
    Trills in Roger were marked by a "t" above the note.  
      The old "+", "x" figured bass notations in Roger were replaced 
    by the easier to read "#" as a figure or an addition to a figure. 
    When "/" & "\" crossed through numbered figures modifying their 
    function, we modified the numbers with a "#" "b" or a natural, as 
    was appropriate - hopefully clarifying the reading of the figures.  
    (The "/" through the figure "7" actually looks like a flat and 
    acts like one.  

    CONCERTO III in G Major, RV 301 

      Movement 1 - Allegro (3/4) 
        Meas.102\1, 106\1, 108\1 - Vn.1 - Roger mssing a sharp. It 
          carries over the bar line. Old notation.  EDITORIAL "#s" 
        Meas.102\1, 108\1 - Vn.2 - Roger mssing a sharp. It carries 
          over the bar line. Old notation.  EDITORIAL "#s" 
        Meas. 95\1 - Va. - Roger missing a sharp.  It carries over 
          the bar line.  Made EDITORIAL "#". 
      
      Movement 2 - Largo (12/8) - in B minor 
        Most of this movement is a solo accompanied by the upper 
          strings.  
        Meas.4 - Bc - Missing # on 1st note. Carries over bar. EDIT.# 
        Meas.6 - Vn.2 -  "    #  "  "  "  "    "      "      " EDIT.# 
        Meas.8 - Vn.Solo - Roger missing naturals on a D5 (10th beat) 
          & the A5 three notes before the end of the measure.  Earlier 
          in the meas. they were #s & harmony changed.  EDIT.naturals.  
        Meas.12 - Vn.Solo - Roger missing # on the 6th beat. Harmonic 
          shift restors previously naturalized C5.  Made EDITORIAL #. 
        Mear.15 - Vn.Solo - Natural missing on 1st note.  Carries over 
          bar line.  Roger missing # on 11th beat. Harmonic change 
          going on in lower strings.  Made EDITORIAL accidentals.  
        Meas.16 - Vn.2 - Missing # on 1st note. Carries over bar. EDIT.# 
        Meas.17 - Vn.1 - Roger has an extra bar line in mid-measure.  

      Movement 3 - Allegro Assai (3/8) 
        Whole measure rests in this movement in Roger are the common 
          dark rectangles hanging from bar lines.  We've entered them 
          as dotted quarter note rests. 
        Meas.32 - Vn.Solo - Sharp not restated on 1st note. EDIT. # 
        Meas.35\2 - Vn.1 - Sharp missing from C5. C#5 in Solo. EDIT. # 
        Meas.36\2 - Vn.2 - Sharp missing from C5. C#5 in Solo. EDIT. # 
        Meas.43 - Vn.2 - Ri_scan missed slur, unusual for Op.4 is full 
          of slurs, ".", and "_" markings). Slur in printed ed. of Ric.  
        Meas.57-8 - Vn.Solo - Naturals missing on F4. Part of a new 
          harmonic progression.  Made EDITORIAL naturals.  
        Meas.85-8 - Vn.Solo - Naturals missing on the F4s in Roger.  
          Harmonically leading to c minor in m.87-88. EDIT. naturals.  
        Meas.103 - Vn.Solo - Flat not restated on 1st note. EDIT. flat.  

      Movement 38 - Allegro Assai (3/8) 
        Whole measure rests in this movement are represented by a 
          dotted quarter note rest.                         (9-30-10) 
