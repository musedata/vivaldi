        NOTES on Vivaldi Opus 4 - Violin Concertos I - XII 
    for Violino di Concertino, Vn.1 & 2, Viola, & Basso continuo.  

  The facsimile of Vivaldi's La Stravaganza Op.4 has five part-books: 
     Violino di Concertino, Violino Prima, Violino Seconda, Alto 
     Viola, and Organo e Violoncello (with figures above the notes).  
     This facsimile is based on the copy of the publication done by 
      Chez Estienne Roger Marchand Libraire, Amsterdam, No.399, 
         which belongs to the Royal Academy of Music, London.  

                         (Oct. 16, 2010) 

      The facsimile had a light double bar between movements, There 
    were no repeat signs in Opua 4, except for the Allegro of 
    Concerto III.  After each Concerto the light double bar was 
    followed by several vertical lines of diminishing heights.   
    There were holds at the end of each movement except for 
    Concertos VIII and X, which to me are particularly interesting,        
    in the way the Adagios functioned differently than the other 
    middle movements.  Concerto VIII was particularly interesting 
    harmonically and in that the middle movement was in three parts, 
    all in common time (C) - the first a one measure Adagio which 
    provided the cadence (with fermata) for the first movement and 
    then a Presto followed by another Adagio, which resolved the 
    Presto in the first measure, ending with a fermata before the 
    final Allegro.  
      "&0" was entered in col.13-14 to indicate that there was 
    editorial material from the source on that line. "&1" = a CCARH 
    editorial where something was changed from the source.  On the 
    lines between "&0" or "&1" in the 1st column there would often be 
    a comment on the situation.  Editorial accidentals which were 
    added appeared in smaller fonts & notes were made in the files.  
    Trills in Roger were marked by a "t" above the note.  
      The old "+", "x" figured bass notations in Roger were replaced 
    by the easier to read "#" as a figure or an addition to a figure. 
    When "/" & "\" crossed through numbered figures modifying their 
    function, we modified the numbers with a "#" "b" or a natural, as 
    was appropriate - hopefully clarifying the reading of the figures.  
    (The "/" through the figure "7" actually looks like a flat and 
    acts like one.  

    CONCERTO IV in A Minor, RV 357 

      Movement 1 - Allegro (C)  
        Meas.11 - Bc. - There is a figure "X" on the A2 on beat 3 in 
          Roger which does not make sense and was left out. There is 
          a sharp missing in Roger on the first note of the Bc. part, 
          which carries over from the previous meas. & is marked as 
          an EDITORIAL sharp.  
        Meas.18 - Vn.Solo - Roger missing flat on 1st 8th of beat 4.  
          but it is implied when the figure 6b appeared on a G2 on 
          beat 3.  The next 8th has a flat.  Entered EDITORIAL "b". 
        Meas.20 - Vn.Solo - The only flat marked on a B4 by Roger in 
          this measure is in the 2nd beat.  There is a chance that 
          Vivaldi thought that next B4 in beat 3 should revert to the 
          key signature but he didn't mark it & it sounds acceptable 
          either way but we entered it like the Roger ed.  Note that 
          that a Bb is marked in meas.21.  
        Meas.33 - Vn.Solo - No "b" on 1st note in Roger. Understood.  
          Carries over bar line.  Entered EDITORIAL flat.  
        Meas.56 - Vn.Solo - No "#" on 1st note but it is tied over 
          the bar line so it is just a cautionary accidental.  
        Meas.66 & 69 - Vn.Solo - Roger has flats in front of the F5s 
          here but these cautionary accidentals seem unnecessary.  
      
      Movement 2 - Grave (3/4)                        
        The Bc. is tacet for this movement.  All parts have "Grave e 
          sempre Piano" written below each staff.  Above the staff of 
          the Vn.Solo is written "Solo e Cantabile".  
        Meas.9\1 - Va. - Roger had a cautionary flat on the B4.  
        Meas.14\1 - Vn.2 & Va. - Accidental missing on 1st note of 
          Roger.  It carries over the bar line _ natural for Vn.2  
          and a sharp for Va.  Made EDITORIAL accidentals. 
        Meas.14\4 - Vn.Solo - Roger has a courtesy "b" on the F5. We 
          entered a natural.  
        Meas.16 - Vn.2 - An editorial flat on the B4 in Roger. 
        Meas.16\1 - Vn.2 - Roger had a cautionary flat on the B4.  
          Va. A4 here makes for an interesting suspension resolving 
          to a G4 in the next measure.  
        Meas.22\1 - Va. -  Roger had a cautionary flat on the B3. 
        Meas.24\1 - Vn.Solo & Meas.25 - Vn.1  Roger missing a sharp 
          on the F5.  Understood.  It carries over the bar. EDIT.#s. 
        Meas.27\1 - Vn.2 - Roger missing a "#" on the C5. Understood.  
          It carries over the bar line.  Entered EDIT #.  

      Movement 3 - Allegro (3/48) 
        Meas.38\1-2 - Vn.1 - Sharps missing on Fs in Roger. Harmony 
          carries over bar line.  Entered EDIT #s.  
        Meas.84 - Vn.Solo & Bc. - Unusual sounding.  Roger had a 
          cautionary natural on the F5.  (F# in the previous meas.).  
          Roger also had a cautionary natural in m.85 (a repeat of 
          m.84) but with modern notation it wasn't necessary in m.85.  
                                                          (10-26-10) 
