(C) 2004, 2019 Center for Computer Assisted Research in the Humanities 
ID: {viv/roger/op1/rv066/stage2/05/01}
TIMESTAMP: DEC/10/2005 [md5sum:8f3bb9a8ce032732e07796bba37ca7e7]
10/04/04 Fran Bennion   rev. 05/12/21 E Correia 
WK#:01,04     MV#:5
RV 66 / Estienne Roger (No. 363), Amsterdam [facs.]
Vivaldi Op. 1, No. 4 in E Major
Movement 5 - Giga
Violino 1

Group memberships: score
score: part 1 of 4
$   Q:4 K:4 T:12/8 C:4   D:Allegro 
E5     2      1 e     d
measure 1
E5     4      1 q     d
B4     2      1 e     d
B4     4      1 q     d
G#5    2      1 e     d
G#5    4      1 q     d
E5     2      1 e     d
E5     4      1 q     d
B5     2      1 e     d
measure 2
B5     2      1 e     d  [
A5     2      1 e     d  =
G#5    2      1 e     d  ]
F#5    2      1 e     d  [
G#5    2      1 e     d  =
E5     2      1 e     d  ]
D#5    2      1 e     d  [
C#5    2      1 e     d  =
D#5    2      1 e     d  ]
B4     2      1 e     d  [
C#5    2      1 e     d  =
D#5    2      1 e     d  ]
measure 3
E5     4      1 q     d
G#4    2      1 e     u
B3     4      1 q     u
D#5    2      1 e     d
E4     6      1 q.    u
rest   4      1 q
rest   2      1 e
measure 4
rest   2      1 e
E5     2      1 e     d  [
D#5    2      1 e     d  ]
E5     2      1 e     d  [
F#5    2      1 e     d  =
G#5    2      1 e     d  ]
C#5   12      1 h.    d
measure 5
rest   2      1 e
F#5    2      1 e     d  [
E5     2      1 e     d  ]
F#5    2      1 e     d  [
G#5    2      1 e     d  =
A5     2      1 e     d  ]
D#5   12      1 h.    d
measure 6
rest   2      1 e
G#5    2      1 e     d  [
F#5    2      1 e     d  ]
G#5    2      1 e     d  [
A5     2      1 e     d  =
B5     2      1 e     d  ]
E5    12      1 h.    d
measure 7
rest   2      1 e
E5     2      1 e     d  [
D#5    2      1 e     d  ]
E5     2      1 e     d  [
D#5    2      1 e     d  =
C#5    2      1 e     d  ]
F#5    4      1 q     d
B4     2      1 e     d
B4     4      1 q     d
A#4    2      1 e #   u
measure 8
B4     2      1 e     d  [
F#5    2      1 e     d  =
E5     2      1 e     d  ]
D#5    2      1 e     d  [
E5     2      1 e     d  =
F#5    2      1 e     d  ]
B4     6      1 q.    d
rest   2      1 e
rest   2      1 e
F#5    2      1 e     d
measure 9
G#5    4      1 q     d        (
P C32:o
C#5    2      1 e     d        )
F#5    4      1 q     d        (
P C32:o
B4     2      1 e     d        )
E5     4      1 q     d        (
P C32:o
A#4    2      1 e #   u        )
B4     6-     1 q.    d        -
measure 10
B4     3      1 e.    d  [
C#5    1      1 s     d  =\
B4     2      1 e     d  ]
F#4    4      1 q     u
A#4    2      1 e #   u
B3     6      1 h     u
&1
In the Roger facsimile the B3 appeared as a half note.  I shortened the note
value so that it would be rhythmically correct when played, but the visual
appearance of this measure remains like the Roger edition in the score.
&1
rest   4      1 q
mheavy4          |: :|
G#5    2      1 e     d
measure 11
G#5    2      1 e     d  [
F#5    2      1 e     d  =
E5     2      1 e     d  ]
D#5    2      1 e     d  [
E5     2      1 e     d  =
C#5    2      1 e     d  ]
B#4    2      1 e #   u  [
G#4    2      1 e     u  =
B#4    2      1 e     u  ]
C#5    2      1 e     u  [
E4     2      1 e     u  =
C#5    2      1 e     u  ]
measure 12
F#4    2      1 e     u  [
B#4    2      1 e #   u  =
C#5    2      1 e     u  ]
G#4    4      1 q     u
B#4    2      1 e #   d         +
C#5    6      1 q.    d
rest   2      1 e
rest   2      1 e
F#5    2      1 e     d
measure 13
F#5    2      1 e     d  [
C#5    2      1 e     d  =
A#4    2      1 e #   d  ]
F#4    2      1 e     d  [
F#5    2      1 e     d  =
E5     2      1 e     d  ]
D#5    2      1 e     d  [
E5     2      1 e     d  =
F#5    2      1 e     d  ]
B4     4      1 q     d
B5     2      1 e     d
measure 14
B5     2      1 e     d  [
F#5    2      1 e     d  =
D#5    2      1 e     d  ]
B4     2      1 e     d  [
B5     2      1 e     d  =
A5     2      1 e     d  ]
G#5    2      1 e     d  [
A5     2      1 e     d  =
B5     2      1 e     d  ]
E5     4      1 q     d
E5     2      1 e     d
measure 15
E5     2      1 e     d  [     (
P C32:o
D#5    2      1 e     d  =
C#5    2      1 e     d  ]     )
C#5    2      1 e     d  [     (
P C32:o
B4     2      1 e     d  =
A4     2      1 e     d  ]     )
A5     2      1 e     d  [     (
P C32:o
G#5    2      1 e     d  =
F#5    2      1 e     d  ]     )
F#5    2      1 e     d  [     (
P C32:o
E5     2      1 e     d  =
D#5    2      1 e     d  ]     )
measure 16
D#5    2      1 e     d  [     (
P C32:o
C#5    2      1 e     d  =
D#5    2      1 e     d  ]     )
B4     4      1 q     d
B4     2      1 e     d
C#5    2      1 e     d  [     (
B4     2      1 e     d  =
A4     2      1 e     d  ]     )
C#5    2      1 e     d  [     (
P C32:o
B4     2      1 e     d  =
C#5    2      1 e     d  ]     )
measure 17
D#5    2      1 e     d  [     (
P C32:o
C#5    2      1 e     d  =
B4     2      1 e     d  ]     )
D#5    2      1 e     d  [     (
P C32:o
C#5    2      1 e     d  =
D#5    2      1 e     d  ]     )
E5     2      1 e     d  [     (
P C32:o
D#5    2      1 e     d  =
C#5    2      1 e     d  ]     )
E5     2      1 e     d  [     (
P C32:o
D#5    2      1 e     d  =
E5     2      1 e     d  ]     )
measure 18
F#5    2      1 e     d  [     (
P C32:o
E5     2      1 e     d  =
D#5    2      1 e     d  ]     )
F#5    2      1 e     d  [     (
P C32:o
E5     2      1 e     d  =
F#5    2      1 e     d  ]     )
G#5    2      1 e     d  [     (
P C32:o
F#5    2      1 e     d  =
E5     2      1 e     d  ]     )
G#5    2      1 e     d  [     (
P C32:o
F#5    2      1 e     d  =
G#5    2      1 e     d  ]     )
measure 19
A5     2      1 e     d  [     (
P C32:o
G#5    2      1 e     d  =
F#5    2      1 e     d  ]     )
B5     4      1 q     d
E5     2      1 e     d
D#5    2      1 e     d  [     (
P C32:o
C#5    2      1 e     d  =
B4     2      1 e     d  ]     )
A5     6-     1 q.    d        -
measure 20
A5     2      1 e     d  [
B5     2      1 e     d  =
G#5    2      1 e     d  ]
G#5    4      1 q     d
F#5    2      1 e     d
E5     6      1 q.    d
rest   4      1 q
rest   2      1 e
measure 21
rest   2      1 e
E5     2      1 e     d  [
D#5    2      1 e     d  ]
E5     2      1 e     d  [
F#5    2      1 e     d  =
G#5    2      1 e     d  ]
C#5   12      1 h.    d
measure 22
rest   2      1 e
F#5    2      1 e     d  [
E5     2      1 e     d  ]
F#5    2      1 e     d  [
G#5    2      1 e     d  =
A5     2      1 e     d  ]
D#5   12      1 h.    d
measure 23
rest   2      1 e
G#5    2      1 e     d  [
F#5    2      1 e     d  ]
G#5    2      1 e     d  [
A5     2      1 e     d  =
B5     2      1 e     d  ]
E5    12      1 h.    d
measure 24
rest   2      1 e
A5     2      1 e     d  [
G#5    2      1 e     d  ]
A5     2      1 e     d  [
B5     2      1 e     d  =
C#6    2      1 e     d  ]
rest   2      1 e
F#5    2      1 e     d  [
E5     2      1 e     d  ]
F#5    2      1 e     d  [
G#5    2      1 e     d  =
A5     2      1 e     d  ]
measure 25
rest   2      1 e
D#5    2      1 e     d  [
C#5    2      1 e     d  ]
D#5    2      1 e     d  [
E5     2      1 e     d  =
F#5    2      1 e     d  ]
B4     4      1 q     d
E5     2      1 e     d
E5     4      1 q     d
D#5    2      1 e     d
measure 26
E5     2      1 e     d  [
B4     2      1 e     d  =
A4     2      1 e     d  ]
G#4    2      1 e     u  [
A4     2      1 e     u  =
B4     2      1 e     u  ]
E4     6      1 q.    u
rest   2      1 e
rest   2      1 e
B4     2      1 e     d
measure 27
C#5    4      1 q     d        (
F#4    2      1 e     u        )
B4     4      1 q     d        (
P C32:o
E4     2      1 e     u        )
A4     4      1 q     u        (
P C32:u
D#4    2      1 e     u        )
E4     6-     1 q.    u        -
measure 28
E4     3      1 e.    u  [
F#4    1      1 s     u  =\
E4     2      1 e     u  ]
B3     4      1 q     u
D#4    2      1 e     u
E4    10      1 h.    u
&1
In the Roger facsimile the E4 appeared as a dotted half note.  I shortened
the note value so that it would be rhythmically correct when played, but the
visual appearance of this measure remains like the Roger edition in the score.
&1
mheavy2          :|
/END
