(C) 2004, 2019 Center for Computer Assisted Research in the Humanities 
ID: {viv/roger/op1/rv073/stage2/02/01}
TIMESTAMP: DEC/10/2005 [md5sum:a4fce51b19190593aa3d092e350b152e]
09/22/04 Fran Bennion  rev. 05/12/21 E Correia 
WK#:01,01     MV#:2
RV 73 / Estienne Roger (No. 363), Amsterdam [facs.]
Vivaldi Op. 1, No. 1 in g minor
Movement 2 - Allemanda
Violino 1

Group memberships: score
score: part 1 of 4
$   Q:4  K:-1(-1)  T:1/1  C:4  D: Allegro 
&
Sonata I is in g minor but Roger has notated it in 1 flat (Op01No01), these
files.  It has been transcribed to 2 flats (modern notation) in Op1No1Gm.
&
D5     2      1 e     d
measure 1
G5     2      1 e     d  [
D5     2      1 e     d  =
A5     2      1 e     d  =
D5     2      1 e     d  ]
Bf5    4      1 q     d
rest   2      1 e
A5     2      1 e     d
measure 2
G5     2      1 e     d  [
D5     2      1 e     d  =
A5     2      1 e     d  =
D5     2      1 e     d  ]
Bf5    6      1 q.    d
G5     2      1 e     d
measure 3
Af5    2      1 e f   d  [     (
F#5    2      1 e #   d  ]     )
G5     8      1 h     d         &2t 
F#5    4      1 q #   d 
measure 4
G5     4      1 q     d
rest   4      1 q
G5     1      1 s     d  [[
Ef5    1      1 s f   d  ==
C5     1      1 s     d  ==
G5     1      1 s     d  ]]
G5     1      1 s     d  [[
Ef5    1      1 s f   d  ==      
C5     1      1 s     d  ==
G5     1      1 s     d  ]]
measure 5
A5     8      1 h     d
F5     1      1 s     d  [[
D5     1      1 s     d  ==
Bf4    1      1 s     d  ==
F5     1      1 s     d  ]]
F5     1      1 s     d  [[
D5     1      1 s     d  ==
Bf4    1      1 s     d  ==
F5     1      1 s     d  ]]
measure 6
G5     1      1 s     d  [[
Ef5    1      1 s f   d  ==
C5     1      1 s     d  ==
G5     1      1 s     d  ]]
A5     1      1 s     d  [[
F5     1      1 s     d  ==
C5     1      1 s     d  ==
A5     1      1 s     d  ]]
Bf5    1      1 s     d  [[
F5     1      1 s     d  ==
Ef5    1      1 s f   d  ==      
D5     1      1 s     d  ]]
C5     3      1 e.    d  [
Bf4    1      1 s     d  ]\
measure 7
Bf4    2      1 e     d  [
F5     2      1 e     d  ]
Bf5    4-     1 q     d        -
Bf5    2      1 e     d  [
Af5    2      1 e f   d  ]
Af5    1      1 s     d  [[
G5     1      1 s     d  ==
F5     1      1 s     d  ==
Af5    1      1 s f   d  ]]      
measure 8
G5     2      1 e     d  [
C5     2      1 e     d  ]
C6     4-     1 q     d        -
C6     2      1 e     d  [
Bf5    2      1 e     d  ]
Bf5    1      1 s     d  [[
A5     1      1 s     d  ==
G5     1      1 s     d  ==
Bf5    1      1 s     d  ]]
measure 9
A5     2      1 e     d  [
D5     2      1 e     d  ]
D6     4-     1 q     d        -
D6     2      1 e     d  [
C6     2      1 e     d  ]
C6     1      1 s     d  [[
Bf5    1      1 s     d  ==
A5     1      1 s     d  ==
C6     1      1 s     d  ]]
measure 10
Bf5    4-     1 q     d        -
Bf5    3      1 e.    d  [
G5     1      1 s     d  ]\
A5     4-     1 q     d        -
A5     3      1 e.    d  [
F5     1      1 s     d  ]\
measure 11
G5     4-     1 q     d        -
G5     3      1 e.    d  [
E5     1      1 s     d  ]\
F5     6      1 q.    d
G5     2      1 e     d
measure 12
Ef5    2      1 e f   d  [     (
C#5    2      1 e #   d  ]     ) 
D5     8      1 h     d         &2t 
C#5    4      1 q #   d 
@  The C#5s in meas. 12 were scanned as C5s
measure 13
D5     1      1 s     u  [[
A4     1      1 s     u  ==
F4     1      1 s     u  ==
D4     1      1 s     u  ]]
D5     4      1 q     d
rest   4      1 q
rest   2      1 e
mheavy4                         :|:
A4     2      1 e     u
measure 14
D5     2      1 e     d  [
A4     2      1 e     d  =
E5     2      1 e     d  =
A4     2      1 e     d  ]
F5     4      1 q     d
rest   2      1 e
E5     2      1 e     d
measure 15
D5     2      1 e     d  [
A4     2      1 e     d  =
E5     2      1 e     d  =
A4     2      1 e     d  ]
F5     4      1 q     d
rest   2      1 e
E5     2      1 e     d
measure 16
D5     4      1 q     d
rest   2      1 e
D5     2      1 e     d
Bf5    4-     1 q     d        -
Bf5    1      1 s     d  [[
Bf5    1      1 s     d  ==
C6     1      1 s     d  ==
Bf5    1      1 s     d  ]]
measure 17
A5     4-     1 q     d        -
A5     1      1 s     d  [[
A5     1      1 s     d  ==
Bf5    1      1 s     d  ==
A5     1      1 s     d  ]]
G5     3      1 e.    d  [
A5     1      1 s     d  =\
F5     3      1 e.    d  =
G5     1      1 s     d  ]\
measure 18
Ef5    3      1 e.f   d  [
F5     1      1 s     d  =\
D5     3      1 e.    d  =
Ef5    1      1 s f   d  ]\      
C5     3      1 e.    d  [
D5     1      1 s     d  =\
Bf4    3      1 e.    d  =
C5     1      1 s     d  ]\
measure 19
A4     2      1 e     d  [
F5     2      1 e     d  =
C5     2      1 e     d  =
F5     2      1 e     d  ]
D5     2      1 e     d  [
F5     2      1 e     d  ]
Bf5    4-     1 q     d        -
measure 20
Bf5    2      1 e     d  [
Af5    2      1 e f   d  ]
Af5    1      1 s     d  [[
G5     1      1 s     d  ==
F5     1      1 s     d  ==
Af5    1      1 s f   d  ]]      
G5     2      1 e     d  [
C5     2      1 e     d  ]
C6     4-     1 q     d        -
measure 21
C6     2      1 e     d  [
Bf5    2      1 e     d  ]
Bf5    1      1 s     d  [[
A5     1      1 s     d  ==
G5     1      1 s     d  ==
Bf5    1      1 s     d  ]]
A5     2      1 e     d  [
D5     2      1 e     d  ]
D6     4-     1 q     d        -
measure 22
D6     2      1 e     d  [
C6     2      1 e     d  ]
C6     1      1 s     d  [[
Bf5    1      1 s     d  ==
A5     1      1 s     d  ==
C6     1      1 s     d  ]]
Bf5    4-     1 q     d        -
Bf5    3      1 e.    d  [
G5     1      1 s     d  ]\
measure 23
A5     4-     1 q     d        -
A5     3      1 e.    d  [
F5     1      1 s     d  ]\
G5     4-     1 q     d        -
G5     1      1 s     d  [[
Bf5    1      1 s     d  ==
A5     1      1 s     d  ==
G5     1      1 s     d  ]]
measure 24
F#5    1      1 s #   d  [[    ( 
E5     1      1 s     d  =]    )
D5     2      1 e     d  ]
rest   2      1 e
G5     2      1 e     d
Af5    2      1 e f   d  [     (
G5     2      1 e     d  =     )
Af5    2      1 e f   d  =     ( 
G5     2      1 e     d  ]     )
measure 25
F#5    2      1 e #   d  [     ( 
A5     2      1 e n   d  =     )+
D5     2      1 e     d  =     (
F#5    2      1 e #   d  ]     ) 
G5     2      1 e     d  [     (
D5     2      1 e     d  =     )
A5     2      1 e     d  =     (
D5     2      1 e     d  ]     )
@  The F#5s in meas. 24 & 25 were scanned as F5s
measure 26
C6     2      1 e     d  [     (
F#5    2      1 e #   d  ]     )
G5     8      1 h     d         &2t 
F#5    4      1 q #   d         
measure 27
G5     4      1 q     d
rest   2      1 e
*               G       p
P    C17:Y58
G5     2      1 e     d
Af5    2      1 e f   d  [     (
G5     2      1 e     d  =     )
Af5    2      1 e f   d  =     ( 
G5     2      1 e     d  ]     )
measure 28
F#5    2      1 e #   d  [     (
A5     2      1 e n   d  =     )+
D5     2      1 e     d  =     (
F#5    2      1 e #   d  ]     ) 
G5     2      1 e     d  [     (
D5     2      1 e     d  =     )
Ef5    2      1 e f   d  =     (
C5     2      1 e     d  ]     )
measure 29
Af4    2      1 e f   u  [     (
F#4    2      1 e #   u  ]     ) 
G4     8      1 h     u         &2t 
F#4    4      1 q #   u 
measure 30
*               G       f
P    C17:Y64
G4     1      1 s     u  [[
D4     1      1 s     u  ==
Bf3    1      1 s     u  ==
G3     1      1 s     u  ]]
G4     4      1 q     u
rest   4      1 q
rest   2      1 e
mheavy2                         :|
/END
