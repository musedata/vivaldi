

            NOTES on Vivaldi Opus 1 - Trio Sonatas I - XII 


    Estienne Roger Marchand Libraire No. 363, Amsterdam [facsimile] 
     from scanned material done by Don Anthony in June & July 2004 
    transferred to CCARH stage2 format by Fran Bennion & into files 
             md\vivaldi\roger\trio_son\op01no01 - op01no12  

      The notes, their values, stem directions, rests, beamings, and 
    the fermatas were scanned with a high degree of accuracy.  The 
    accidentals were not always scanned accurately, sharps often 
    being scanned as naturals (i.e. No. 1, Mvt. 2).   Fermata 
    placement often needed to be adjusted with print suggestions. 

      Dynamics were scanned as Musical Directions with "*" in col.1, 
    "G" in column 17, & the dynamic in column 25.  Vertical placement 
    was adjusted in "ss2ed". When viewed in the score, the horizontal 
    placement did not always make the best sense.  Roger also had 
    many fewer dynamic markings than the scanned edition.  

      There were many more slurs in the scanned edition than in the 
    facsimile. The scanner picked up the Print suggestions "P C32:o" 
    or "u" for each one.  Roger had more cautionary accidentals than 
    the scanned edition, except for the 2nd note in an octave jump 
    when Roger didn't restate the accidental (No.1, Mvt.4, m.17).  

      The headings (rows 4-9) were redone and notes (i.e. the fact 
    that Op.1 No.1 was in g minor but was notated in one flat) were 
    added.  Tempo markings had to be added (i.e.  D:Allegro).  An 
    "S" appeared at the beginning of line 13 just below the "$" in 
    most files and was removed.  

      The facsimile had a light double bar between movements, but in 
    movements where there are repeats (i.e. No. 1, Mvts. 2, 4, & 5) 
    their repeat signs are like our heavy double bars.  At the end 
    of movements where the second part was to be repeated there was 
    a heavy ":|:" and there was also a light double bar to end the 
    movement.  I just entered our usual light-heavy double bar to 
    end movements.  Repeat signs in the middle of movements were not 
    accurately scanned and when a ":|:" appeared near the end of a 
    measure, i.e. just before the last 8th, an extra measure of an 
    8th note was created (i.e. No. 1, Mvt. 2).  

      When there was a pick up measure it was scanned as measure 1, 
    so the measure numbers had to be changed for that movement.  

      The "/" through the figure "7" actually looks like a flat and 
    acts like one (i.e. No. 1, Mvt. 1, meas.3 & Mvt.4, meas.9). 
    The "\" through the figure "6" is more normal and acts like a + 
    (i.e. No. 1, Mvt.3, meas.2).  A "\" in conjunction with a 5 or a 
    9 didn't print so I used "5+" (No. 2, Mvt. 1, measure 5 amd             
    Mvts. 2-4) and "9+" (No. 2, Mvt. 1, measure 6).  



    SONATA VI in D Major, RV 62 

      Movement 1 - Preludio, Largo (C) 
        Meas. 11 - Violino 1 - The note values in the last half of 
          this measure were scanned wrong. Roger had a dotted 16th, 
          a 32nd, a dotted 16th, a 32nd, a dotted 8th, and a 16th.  
          In the scan the second of these notes was scanned as a 
          16th instead of a 32nd and then to even out the counting 
          at the end an i-rest was added to complete the measure. 

      Movement 2 - Corrente, Allegro (3/4) 
        The scan picked up "mheavy2 26    :|"  and 
        Meas. 26 & 27      "mheavy3 27    |:" in all parts.  
        Roger had only     "mheavy4 26   :|:" in all parts 

      Movement 3 - Adagio (C) 
        Meas. 1 - Vc. & Bc. - Scan missed the sharp on the E#4.  
        Meas. 9 - Violino 2 - First two notes scanned as quarters.  
          Roger had a dotted quarter and an 8th note.  
        Meas. 14 - Violino 2, Vc. & Bc. - No fermata on last note 
          in Roger.  Fermata in Violino 1 and in all parts of the 
          scanned edition.  

      Movement 4 - Allemanda, Allegro (C) 
        The scan picked up   "mheavy2 11    :|"  and 
          Meas. 26 & 27      "mheavy3 12    |:" in all parts.  
          Roger had only     "mheavy4 11   :|:" in all parts 
        Meas. 21 - Vc. & Bc. - Third beat scanned as a quarter note 
          Roger had two 8ths.  The scan picked up the pitch of the 
          2nd 8th. (Usually the pitch of the first note is scanned.) 
        Meas. 23 - Violino 1 - Scan had "p" on second 8th note. In 
          Roger it was on the first 8th note.  
            Violino 2 - "p" scanned after a quarter and an 8th rest.  
              In Roger it was on the first beat.  
            Vc. & Bc. - "p" scanned  after the 1st three 8ths in 
              both editions.  
          Roger didn't have a "forte" in the middle of the 
              movement like the scanned edition.  
        Meas. 25 - All parts - No fermata scanned on the last note. 
