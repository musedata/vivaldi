(C) 2001 Center for Computer Assisted Research in the Humanities.
ID: {viv/micro/op10/rv501/stage2/01/05}
TIMESTAMP: DEC/10/2005 [md5sum:07481e793944b5c3a29b83d4ce645949]
06/27/01 Fran Bennion
WK#:10,2c     MV#:1
RV 501 / Giordano 31 pp.64-71, Torino
Concerto for Bassoon in Bb Major, Op.10, No. 2c, \0"Night"

Fagotto
0 0
Group memberships: parts
parts: part 1 of 5
&1
This varient of Op.10 No.2 is for Bassoon and Strings.
In Dec.08, when recreating files for the lost parts of our Op.10 Dover ed., F.B. altered a
number of things in the Giordano ed. to agree with our printed score to create these files.
The cue size notes & the editorial accidentals in our Dover score (frequently unclear or in
old notation in the original manuscript) are displayed as regular size notes in the parts.
&1
Pv4.02
$  K:-2   Q:24   T:1/1  C:22                          
*               D +     Largo                 
P    C17:Y-30x-9    C25:f34 
P    C0:t2
Bf3   18        e.    d  [
Bf2    6        s     d  =\
Bf2   18        e.    d  =
D4     6        s     d  ]\
D4    18        e.    d  [
Bf2    6        s     d  ]\
Bf2   12-       e     u  [     -
Bf2    3        t     u  =[[
C3     3        t     u  ===
D3     3        t     u  ===
Ef3    3        t     u  ]]]
measure 2
F3    48        h     d         F
G3    18        e.    d  [
Ef3    6        s     d  =\
Ef3   18        e.    d  =
Ef4    6        s     d  ]\
measure 3
Ef4   18        e.    d  [
A2     6        s     d  ]\
A2    12-       e     d  [     -
A2     3        t     d  =[[
F3     3        t     d  ===
G3     3        t     d  ===
A3     3        t     d  ]]]
gA3    6        e     u        (
Bf3   48        h     d        )F
P    C33:Y-28
measure 4
Bf2   18        e.    u  [
Bf2    6        s     u  =\
Bf2   18        e.    u  =
Bf2    6        s     u  ]\
Bf2   18        e.    u  [
Bf2    6        s     u  =\
Bf2   18        e.    u  =
Bf2    6        s     u  ]\
measure 5
Ef3   18        e.    u  [
C3     6        s     u  =\
C3    18        e.    u  =
C3     6        s     u  ]\
C3    18        e.    u  [
C3     6        s     u  =\
C3    18        e.    u  =
C3     6        s     u  ]\
measure 6
F3    18        e.    d  [
D3     6        s     d  =\
D3    18        e.    d  =
D3     6        s     d  ]\
D3    18        e.    d  [
D3     6        s     d  =\
D3    18        e.    d  =
D3     6        s     d  ]\
measure 7
G3    18        e.    d  [
Ef3    6        s     d  =\
Ef3   18        e.    d  =
Ef3    6        s     d  ]\
Ef3   18        e.    d  [
Ef3    6        s     d  =\
Ef3   18        e.    d  =
Ef3    6        s     d  ]\
measure 8
F3    24        q     d         F
rest   3        t
F3     3        t     d  [[[
G3     3        t     d  ===
A3     3        t     d  ===
Bf3    3        t     d  ===
C4     3        t     d  ===
D4     3        t     d  ===
Ef4    3        t     d  ]]]
F4    12        e     d
rest   6        s
F3     6        s     d  [/
F3    18        e.    d  =
F3     6        s     d  ]\
measure 9
Ef3   24        q     d
rest   3        t
Ef3    3        t     d  [[[
F3     3        t     d  ===
G3     3        t     d  ===
A3     3        t     d  ===
Bf3    3        t     d  ===
C4     3        t     d  ===
D4     3        t     d  ]]]
Ef4   12        e     d
rest   6        s
Ef3    6        s     d  [/
Ef3   18        e.    d  =
Ef3    6        s     d  ]\
measure 10
D3    24        q     d
rest   3        t
D3     3        t     d  [[[
Ef3    3        t     d  ===
F3     3        t     d  ===
G3     3        t     d  ===
A3     3        t     d  ===
Bf3    3        t     d  ===
C4     3        t     d  ]]]
D4    12        e     d
rest   6        s
D3     6        s     u  [/
C3    18        e.    u  =
Bf2    6        s     u  ]\
mdouble 11
P    C1:]
*               D +     Andante molto
P    C17:Y-35x-9    C25:f34 
F3    48        h     d         F
Bf3    4        s  3  d  [[     *
F3     4        s  3  d  ==
Bf3    4        s  3  d  ]]     !
D4     4        s  3  d  [[     *
Bf3    4        s  3  d  ==
D4     4        s  3  d  ]]     !
Bf3    4        s  3  d  [[     *
F3     4        s  3  d  ==
Bf3    4        s  3  d  ]]     !
F3     4        s  3  d  [[     *
D3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
measure 12
Af2   12        e f   u  [
Af2   12        e     u  =
Af2   12        e     u  =
Af2   12        e     u  ]
G2    24        q     u
rest  24        q
measure 13
C4     4        s  3  d  [[     *
G3     4        s  3  d  ==
C4     4        s  3  d  ]]     !
E4     4        s n3  d  [[     *
C4     4        s  3  d  ==
E4     4        s  3  d  ]]     !
C4     4        s  3  d  [[     *
G3     4        s  3  d  ==
C4     4        s  3  d  ]]     !
G3     4        s  3  d  [[     *
E3     4        s n3  d  ==
G3     4        s  3  d  ]]     !
Bf2   12        e     u  [
Bf2   12        e     u  =
Bf2   12        e     u  =
Bf2   12        e     u  ]
measure 14
A2    24        q     u
rest  24        q
F3     4        s  3  d  [[     *
C3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
A3     4        s  3  d  [[     *
F3     4        s  3  d  ==
A3     4        s  3  d  ]]     !
C4     4        s  3  d  [[     *
A3     4        s  3  d  ==
C4     4        s  3  d  ]]     !
Ef4    4        s  3  d  [[     *
C4     4        s  3  d  ==
Ef4    4        s  3  d  ]]     !
measure 15
D4     4        s  3  d  [[     *
Bf3    4        s  3  d  ==
D4     4        s  3  d  ]]     !
Bf3    4        s  3  d  [[     *
F3     4        s  3  d  ==
Bf3    4        s  3  d  ]]     !
F3     4        s  3  d  [[     *
D3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
D3     4        s  3  u  [[     *
Bf2    4        s  3  u  ==
D3     4        s  3  u  ]]     !
F3     4        s  3  d  [[     *
C3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
A3     4        s  3  d  [[     *
F3     4        s  3  d  ==
A3     4        s  3  d  ]]     !
C4     4        s  3  d  [[     *
A3     4        s  3  d  ==
C4     4        s  3  d  ]]     !
Ef4    4        s  3  d  [[     *
C4     4        s  3  d  ==
Ef4    4        s  3  d  ]]     !
measure 16
D4     4        s  3  d  [[     *
Bf3    4        s  3  d  ==
D4     4        s  3  d  ]]     !
Bf3    4        s  3  d  [[     *
F3     4        s  3  d  ==
Bf3    4        s  3  d  ]]     !
F3     4        s  3  d  [[     *
D3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
D3     4        s  3  u  [[     *
Bf2    4        s  3  u  ==
D3     4        s  3  u  ]]     !
F3    48        h     d         F
measure 17
C3     4        s  3  u  [[     *
A2     4        s  3  u  ==
C3     4        s  3  u  ]]     !
Ef3    4        s  3  u  [[     *
C3     4        s  3  u  ==
Ef3    4        s  3  u  ]]     !
A3     4        s  3  d  [[     *
F3     4        s  3  d  ==
A3     4        s  3  d  ]]     !
Ef4    4        s  3  d  [[     *
C4     4        s  3  d  ==
Ef4    4        s  3  d  ]]     !
Bf3    4        s  3  d  [[     *
F3     4        s  3  d  ==
Bf3    4        s  3  d  ]]     !
F3     4        s  3  d  [[     *
D3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
D3     4        s  3  u  [[     *
Bf2    4        s  3  u  ==
D3     4        s  3  u  ]]     !
Bf2    4        s  3  u  [[     *
F2     4        s  3  u  ==
Bf2    4        s  3  u  ]]     !
measure 18
C3     4        s  3  u  [[     *
A2     4        s  3  u  ==
C3     4        s  3  u  ]]     !
Ef3    4        s  3  u  [[     *
C3     4        s  3  u  ==
Ef3    4        s  3  u  ]]     !
A3     4        s  3  d  [[     *
Ef3    4        s  3  d  ==
A3     4        s  3  d  ]]     !
C4     4        s  3  d  [[     *
A3     4        s  3  d  ==
C4     4        s  3  d  ]]     !
Bf3    4        s  3  d  [[     *
F3     4        s  3  d  ==
Bf3    4        s  3  d  ]]     !
F3     4        s  3  d  [[     *
D3     4        s  3  d  ==
F3     4        s  3  d  ]]     !
D3     4        s  3  u  [[     *
Bf2    4        s  3  u  ==
D3     4        s  3  u  ]]     !
Bf2    4        s  3  u  [[     *
F2     4        s  3  u  ==
Bf2    4        s  3  u  ]]     !
measure 19
F3    96        w     d         F
mdouble 
/END
