(C) 2000 Center for Computer Assisted Research in the Humanities.
ID: {viv/micro/op10/rv442/stage2/02/01}
TIMESTAMP: DEC/10/2005 [md5sum:53d3c892f7d985c172dd2e0b7265d5e2]
06/26/00 Fran Bennion
WK#:10,5a     MV#:2
RV 442 / Giordano 31 pp.347-52, Torino
Vivaldi's Op. 10, No 5 var., in F major
Concerto for Recorder
Recorder
0 0
Group memberships: parts
parts: part 1 of 4
&1
This file was derived from the above (see files 01-04, written
out in 2 flats, starting with a pick up in the violins on D4.)
They were the only files available to me when I was redoing the
lost parts of our Dover Op.10 ed. in Jan.'09.  The main differences
from the above edition were that it needed to be transposed, there was
a Da Capo that needed to be written out, & some modification of
instrumental titles was necessary to fit the Dover edition.
The next two lines are notes from the above files.
"Write one tone higher and in five staves." - Vivaldi
Manuscript in three flats and starts with a pick up on C4 in the violins.
&1
Pv4.02
$  K:-4   Q:4   T:12/8  C:4  D:Largo e cantabile
rest   2        e
measure 1
rest  24
measure 2
rest  24
measure 3
rest  24
measure 4
rest  24
measure 5
rest  24
measure 6
rest   6        q.
rest   4        q
C5     2        e     d
F5     3        e.    d  [
Af5    1        s     d  =\
G5     2        e     d  ]
F5     4        q     d
F5     2        e     d
measure 7
F5     3        e.    d  [
Af5    1        s     d  =\
G5     2        e     d  ]
F5     4        q     d
F5     2        e     d
F5     3        e.    d  [
Af5    1        s     d  =\
Bf5    2        e     d  ]
C6     6-       q.    d        -
measure 8
C6     2        e     d  [
Bf5    2        e     d  =
Af5    2        e     d  ]
G5     2        e     d  [
Af5    2        e     d  =
Bf5    2        e     d  ]
gF5    6        e     u        (
E5     6        q.    d        )
@1   No # on F5 in Le Cene.
rest   4        q
rest   2        e
measure 9
Af5    3        e.    d  [
G5     1        s     d  =\
F5     2        e     d  ]
Ef5    4        q f   d         +
Ef5    2        e     d
Af5    3        e.    d  [
G5     1        s     d  =\
F5     2        e     d  ]
Ef5    4        q     d
Ef5    2        e     d
measure 10
Af5    2        e     d  [
C5     2        e     d  =
Df5    2        e     d  ]
Ef5    4        q     d
F5     2        e     d
gG5    6        e     u        (
F5     2        e     d        )
Ef5    4-       q     d        -
Ef5    4        q     d
Ef5    2        e     d
measure 11
Af5    2        e     d  [
C5     2        e     d  =
Df5    2        e     d  ]
Ef5    4        q     d
F5     2        e     d
gG5    6        e     u        (
F5     2        e     d        )
Ef5    4-       q     d        -
Ef5    4        q     d
Af5    2        e     d
measure 12
G5     4        q     d
Af5    2        e     d
C6     4        q     d
Bf5    2        e     d
Af5    6        q.    d
rest   4        q
F5     2        e     d
measure 13
F5     3        e.    d  [
A5     1        s n   d  =\
G5     2        e     d  ]
F5     4        q     d
F5     2        e     d
A5     2        e     d  [
Bf5    2        e     d  =
C6     2        e     d  ]
Ef5    4        q     d
Ef5    2        e     d
measure 14
A5     2        e n   d  [
Bf5    2        e     d  =
C6     2        e     d  ]
Ef5    4        q     d
Ef5    2        e     d
Ef5    4        q     d
Df5    2        e     d
rest   4        q
F5     2        e     d
measure 15
F5     3        e.    d  [
Af5    1        s f   d  =\     +
G5     2        e     d  ]
F5     4        q     d
F5     2        e     d
B5     2        e n   d  [
C6     2        e     d  =
D6     2        e n   d  ]
F5     4        q     d
F5     2        e     d
measure 16
B5     2        e n   d  [
C6     2        e     d  =
D6     2        e n   d  ]
F5     4        q     d
F5     2        e     d
F5     4        q     d
E5     2        e n   d
E5     4        q n   d
rest   2        e
measure 17
G5     3        e.    d  [
F5     1        s     d  =\
G5     2        e     d  ]
C5     4        q     d
C5     2        e     d
G5     3        e.    d  [
Af5    1        s     d  =\
G5     2        e     d  ]
C6     4        q     d
G5     2        e     d
measure 18
C5    12-       h.    d        -
C5     2        e     d  [
C6     2        e     d  =
Bf5    2        e     d  ]
Af5    2        e     d  [
G5     2        e     d  =
F5     2        e     d  ]
measure 19
C5    12-       h.    d        -
C5     2        e     d  [
C6     2        e     d  =
Bf5    2        e     d  ]
Af5    2        e     d  [
G5     2        e     d  =
F5     2        e     d  ]
measure 20
E5     2        e n   d  [
D5     2        e n   d  =
C5     2        e     d  ]
rest   4        q
rest   2        e
Bf5    3        e.    d  [
C6     1        s     d  =\
Bf5    2        e     d  ]
Bf5    4        q     d
Bf5    2        e     d
measure 21
Bf5    6        q.    d        (
Af5    6        q.    d        )
Gf5    3        e.f   d  [
Af5    1        s     d  =\
Gf5    2        e     d  ]
Gf5    4        q     d
F5     2        e     d
measure 22
E5     6        q.n   d
C6     6        q.    d
E5     4        q n   d
F5     2        e     d
Af5    4        q     d        (
G5     2        e     d        )
measure 23
F5    12        h.    d
Gf5    6        q.f   d
A5     6        q.n   d
measure 24
Bf5    3        e.    d  [
C6     1        s     d  =\
Bf5    2        e     d  ]
Bf5    4        q     d
Af5    2        e     d
Gf5    6        q.f   d
F5     6        q.    d
measure 25
E5     3        e.n   d  [
F5     1        s     d  =\
E5     2        e n   d  ]
F5     4        q     d
G5     2        e     d
Bf4    6-       q.    d        -
Bf4    4        q     d
Bf5    2        e     d
measure 26
E5     4        q n   d
F5     2        e     d
Af5    4        q     d
G5     2        e     d
F5     6-       q.    d        -
F5     4        q     d
rest   2        e
measure 27
rest  24
measure 28
rest  24
measure 29
rest  24
measure 30
rest  24
measure 31
rest  24
measure 32
rest   6        q.              F
rest   6        q.
rest  12        h.
mheavy2
/END
